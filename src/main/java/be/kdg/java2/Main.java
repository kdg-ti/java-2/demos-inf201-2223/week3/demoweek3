package be.kdg.java2;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) {
//        CodeAnalyzer analyzer = new CodeAnalyzer();
//        analyzer.analyseClass(Student.class);
//        Student student = new Student(123, "jos", 1.89);
//        analyzer.runGetters(student);
//        analyzer.createObject(Student.class);
//        analyzer.createObject(Main.class);
//        Student student1 = new Student(123, "jef",1.89);
//        Class clazz = student1.getClass();
//        try {
//            Field field = clazz.getDeclaredField("id");
//            field.setAccessible(true);
//            field.set(student1, 456);
//            System.out.println(student1);
//        } catch (NoSuchFieldException e) {
//            throw new RuntimeException(e);
//        } catch (IllegalAccessException e) {
//            throw new RuntimeException(e);
//        }
        CodeAnalyzer codeAnalyzer = new CodeAnalyzer();
        codeAnalyzer.showNotFinishedMethods(Student.class);
    }
}
