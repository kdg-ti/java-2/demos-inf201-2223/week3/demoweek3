package be.kdg.java2;

//@NotFinished
public class Student {
    private final int id;
    private String name;
    private double length;

    public Student(int id, String name, double length) {
        this.id = id;
        this.name = name;
        this.length = length;
    }

    public Student(int id) {
        this(-1, "dummy",0);
    }

    public Student() {
        this(-1);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @NotFinished(developer = "hans vochten")
    public void setName(String name) {
        this.name = name;
    }

    public double getLength() {
        return length;
    }

    @Deprecated
    public void setLength(double length) {
        this.length = length;
    }

    @Override

    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", length=" + length +
                '}';
    }
}
