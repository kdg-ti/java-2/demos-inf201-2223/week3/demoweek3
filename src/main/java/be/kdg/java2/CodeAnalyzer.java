package be.kdg.java2;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class CodeAnalyzer {
    public void analyseClass(Class clazz) {
        System.out.println("Analyzing class " + clazz.getName());
        System.out.println("====================================");
        System.out.println("The methods:");
        Arrays.stream(clazz.getDeclaredMethods())
                .forEach(method -> {
                    System.out.println(method.getName());
                    System.out.println("Number of arguments:" + method.getParameterCount());
                });
        System.out.println("====================================");
        System.out.println("All getters:");
        Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.getName().startsWith("get"))
                .forEach(method -> {
                    System.out.println(method.getName());
                });
        System.out.println("====================================");
        System.out.println("All fields:");
        Arrays.stream(clazz.getDeclaredFields())
                .forEach(field -> {
                    System.out.print(field.getName());
                    System.out.println(", type: " + field.getType());
                });
    }

    public void runGetters(Object object) {
        Class clazz = object.getClass();
        Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.getName().startsWith("get"))
                .forEach(method -> {
                    try {
                        System.out.println("Output of " + method.getName()
                                + ": " + method.invoke(object));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    public void createObject(Class clazz){
        Arrays.stream(clazz.getDeclaredConstructors())
                .filter(constructor -> constructor.getParameterCount()==0)
                .forEach(constructor -> {
                    try {
                        Object object = constructor.newInstance();
                        System.out.println(object);
                    } catch (InstantiationException e) {
                        throw new RuntimeException(e);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    public void showNotFinishedMethods(Class clazz){
        Arrays.stream(clazz.getDeclaredMethods())
                .forEach(method -> {
                    NotFinished notFinished = method.getAnnotation(NotFinished.class);
                    if (notFinished!=null) {
                        System.out.println("Method: " + method.getName() + " not finished!");
                        System.out.println(notFinished.developer() + " do your job!!!");
                    }
                });
    }
}
