Demo Week 3
-----------
Reflection:
- je kan van een class de methodes, fields, constructors, ... opvragen
- Je kan die verder analyzeren: bv parameters opvragen van methods
- Je kan die methodes dan ook runnen door gebruik te maken van invoke
- Je kan objecten aanmaken door de constructor op te vragen en daarop getInstance te doen
- Je kan zelfs private fields even public maken en aanpassen. Dan ben je wel een beetje aan het hacken...

Annotations:
- in Java zitten een aantal annotations die nuttige extra info toevoegen en door tools zoals IntelliJ en de javac compiler gebruikt worden
- Veel frameworks en libraries hebben ook hu eigen annotaties die we in de toekomst gaan gebruiken
- Je kan zelf annotations maken, we maken een NotFinished annotation
- Je kan annotations ook uitlezen via reflection, dan moet Retention wel op RUNTIME staan in de annotation
- Je kan parameters toevoegen aan annotations, de syntax is een beetje interface achtig
- Je kan beperken waar annotations gebruikt kunnen worden, bijvoorbeeld enkel bij methods.